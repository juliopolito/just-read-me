# Core

[[_TOC_]]

## 1. Introducción

**Core** es una librería que tiene la finalidad de incluir todas las herramientas necesarias para el desarrollo de microservicios basados en Java con un fuerte énfasis en la implementación de buenas prácticas de programación. Al actualizarse periódicamente, la probabilidad de entregar un proyecto con vulnerabilidades en las funciones más básicas como conexión a base de datos, impresiones de logs o conectividad, se reduce considerablemente.

Esta librería está desarrollada por:
- **Dirección de Cobranza: _Chapter de Cloud & DevOps_**

## 2. Lenguaje

El lenguaje utilizado para este proyecto se encuentra en `Java` por medio de [OpenJDK 11](https://openjdk.org/projects/jdk/11/).

## 3. Framework
El framework implementado para esta plantilla se basa en [SpringBoot](https://spring.io/projects/spring-boot)

    id 'org.springframework.boot' version '2.5.1'

## 4. Configuración

### 4.1 build.gradle

A continuación se presenta la propuesta:

```gradle
buildscript {
	repositories {
		mavenCentral()
		maven {url 'https://repo.spring.io/snapshot' }
		maven {url 'https://repo.spring.io/milestone' }
		maven {url 'https://repo.maven.apache.org/maven2'}
		maven {url 'https://maven.oracle.com'}
		maven {url 'https://mvnrepository.com/artifact'}
		maven {url "https://plugins.gradle.org/m2/"}
		maven {
			url project.property("artifactory_repository")
			allowInsecureProtocol  = project.property("artifactory_secure")
			credentials{
				username = project.property("artifactory_user")
				password = project.property("artifactory_password")
			}
			name = project.property("artifactory_name")
		}
	}
	dependencies {
		//Check for the latest version here: http://plugins.gradle.org/plugin/com.jfrog.artifactory
		classpath "org.jfrog.buildinfo:build-info-extractor-gradle:4.21.0"
	}
}

plugins {
	id "com.jfrog.artifactory" version "4.21.0"
	id 'org.springframework.boot' version '2.5.1'
	id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	id 'java'
	id 'jacoco'
	id 'java-library'
	id 'maven-publish'
}

group =  project.property("project.group")
version =  project.property("py.version")
sourceCompatibility = '11'
targetCompatibility = '11'


allprojects {
	apply plugin: 'maven-publish'
	apply plugin: "com.jfrog.artifactory"
	apply plugin: 'java'
	apply plugin: 'groovy'
}

java {
	withJavadocJar()
	withSourcesJar()
}

configurations {
	compileOnly {
		extendsFrom annotationProcessor
	}
	all {
		exclude group:'org.springframework.boot', module:'spring-boot-starter-logging'
	}
}

jacoco {
	toolVersion = "0.8.6"
}
test {
	useJUnitPlatform()
	finalizedBy jacocoTestReport // report is always generated after tests run
}
jacocoTestReport {
	dependsOn test // tests are required to run before generating the report
	reports {
		xml.enabled true
		csv.enabled false
		html.destination layout.buildDirectory.dir('jacocoHtml').get().asFile
	}
}

repositories {
	mavenCentral()
	add buildscript.repositories.getByName("devops")
	maven { url 'https://repo.spring.io/snapshot' }
	maven { url 'https://repo.spring.io/milestone' }
	maven {url 'https://repo.maven.apache.org/maven2'}
	maven {url 'https://maven.oracle.com'}
	maven {url 'https://mvnrepository.com/artifact'}
}

artifactory {
	artifactory_contextUrl  = project.property("artifactory_contextUrl")   //The base Artifactory URL if not overridden by the publisher/resolver
	publish {
		repository {
			repoKey = project.property("artifactory_snapshot")
			username = project.property("artifactory_user")
			password = project.property("artifactory_password")
			maven = true

		}
		defaults {
			publications ('mavenJava')
		}
	}
	resolve {
		repository {
			repoKey = project.property("artifactory_public")
			username = project.property("artifactory_user")
			password = project.property("artifactory_password")
			maven = true

		}
	}
}

bootJar{
	enabled=false
}

jar {
	enabled = true
	archiveFileName =project.property("project.name")+"-"+project.property("py.version")+".jar"
	archiveClassifier=""
}

publishing {
	publications {
		mavenJava(MavenPublication) {
			groupId project.property("project.group")
			artifactId project.property("project.name")
			version project.property("py.version")
			//from components.web  //for wars
			from components.java  //for jars

		}
	}
	repositories{
		add buildscript.repositories.getByName("devops")
	}
}

javadoc {
	if(JavaVersion.current().isJava9Compatible()) {
		options.addBooleanOption('html5', true)
	}
}

dependencies {
	// https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt
	implementation group: 'io.jsonwebtoken', name: 'jjwt', version: '0.9.1'
	// https://mvnrepository.com/artifact/com.google.code.gson/gson
	implementation group: 'com.google.code.gson', name: 'gson', version: '2.8.6'
	// https://mvnrepository.com/artifact/org.jacoco/jacoco-maven-plugin
	implementation group: 'org.jacoco', name: 'jacoco-maven-plugin', version: '0.8.6'
	// https://mvnrepository.com/artifact/org.apache.maven/maven-core
	implementation group: 'org.apache.maven', name: 'maven-core', version: '3.8.1'
	// https://mvnrepository.com/artifact/org.springframework/spring-web
	implementation group: 'org.springframework', name: 'spring-web', version: '5.3.6'
	// https://mvnrepository.com/artifact/io.springfox/springfox-swagger2
	implementation group: 'io.springfox', name: 'springfox-swagger2', version: '3.0.0'
	// https://mvnrepository.com/artifact/org.junit.platform/junit-platform-runner
	testImplementation group: 'org.junit.platform', name: 'junit-platform-runner', version: '1.7.1'
	// https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine
	testImplementation group: 'org.junit.jupiter', name: 'junit-jupiter-engine', version: '5.7.1'
	// https://mvnrepository.com/artifact/io.springfox/springfox-swagger-ui
	implementation group: 'io.springfox', name: 'springfox-swagger-ui', version: '3.0.0'
	implementation 'org.springframework.boot:spring-boot-starter-web'

	implementation group: 'org.springframework.boot', name: 'spring-boot-starter-log4j2', version: '2.6.1'
	implementation(group: 'org.springframework.boot', name: 'spring-boot-starter-web', version: '1.3.6.RELEASE'){
		exclude group:'org.springframework.boot', module:'spring-boot-starter-logging'
	}

	// https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-jdbc
	implementation group: 'org.springframework.boot', name: 'spring-boot-starter-jdbc', version: '2.4.5'

	// https://mvnrepository.com/artifact/com.oracle.ojdbc/ojdbc8
	implementation group: 'com.oracle.ojdbc', name: 'ojdbc8', version: '19.3.0.0'

	annotationProcessor 'org.projectlombok:lombok'
	developmentOnly 'org.springframework.boot:spring-boot-devtools'
	compileOnly 'org.projectlombok:lombok'
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
}

tasks.withType(JavaCompile) {
	options.encoding = 'UTF-8'
}

```

### 4.2 gradle.properties

```javascript
project.group=mx.com.bancoazteca.cloud
py.version=1.0.19-SNAPSHOT
project.name=core
project.description=Proyecto core de Cobranza Cloud & Devops, dependencia principal
artifactory_user=jenkins
artifactory_password=GrupoSalinas2018*
artifactory_contextUrl=http://devops:8082/artifactory/
artifactory_snapshot=maven-snapshot
artifactory_releases=maven-releases
artifactory_public=maven-public
artifactory_repository=http://devops:8082/artifactory/maven-public
artifactory_secure=false
artifactory_name=devops
#systemProp.https.proxyHost=localhost
#systemProp.https.proxyPort=3128
systemProp.https.proxyHost=devops
systemProp.https.proxyPort=3128
```

### 4.3 settings.gradle

```gradle
pluginManagement {
	repositories {
		maven { url 'https://repo.spring.io/milestone' }
		maven { url 'https://repo.spring.io/snapshot' }
		gradlePluginPortal()
	}
}
rootProject.name = 'core'
```

## 5. Características del Core

Dentro de las funcionalidades de la librería se destacan principalmente las siguientes:

- **`Base de datos`**
- **`Logs`**
- **`Seguridad`**
- **`Cobranza`**
- **`APIGEE`**
- **`httpClient`**

### 5.1 Base de datos

Se proporcionan funciones para la construcción de callable statements, cursores y diversas utilerías como validación de tipos, conversión de objetos map a json y obtención de tipos de datos.

Dentro de este apartado tenemos la siguiente estructura:

- :open_file_folder: **db**
    - :file_folder: **aedc**
        - :coffee: OracleDao.java
        - :coffee: OracleUtil.java
    - :coffee: Consultas.java
    - :coffee: Utilerias.java

#### :coffee: **aedc/OracleDao.java**

| Características | Descripción |
| ---             | ---         |
| Nombre de clase | OracleDao |
|Descripción|Clase modelo para la manipulación de objetos OracleDAO|
|call(dataSource, in, function. out, idTransaction)|Realiza la ejecucion de una funcion definiendo un Bean de propiedades para la salida.|
|call(dataSource, in, function, rowMapper, idTransaction)|Realiza la ejecucion de una funcion definiendo un RowMapper para la salida.|
|prepareInput(in)|Prepara el input de los parametros de una funcion y determina que tipo de dato SQL de entrada es el objeto que se analiza|

#### :coffee: **aedc/OracleUtil.java**

| Características | Descripción |
| ---             | ---         |
| Nombre de clase | OracleUtil |
|Descripción|Clase modelo para la manipulación de objetos Oracle|
|getType(value)|Metodo que regresa el tipo de dato representado con un entero|

#### :coffee: **Consultas.java**

| Características | Descripción |
| ---             | ---         |
| Nombre de clase | Consultas |
|Descripción|Clase utileria para el manejo de las consultas hacia las bases de datos.|
|consulta(daos)|Método consulta utilizado para invocar querys o funciones db y retornar resultados en modo map|
|buildCS(daos, con)|Método para el manejo de los call statement para consumos hacia db|
|setParametros(cs, parametro)||


#### :coffee: **Utilerias.java**

| Características | Descripción |
| ---             | ---         |
| Nombre de clase | Utilerias |
|Descripción|Clase utileria para el manejo de lobs objetos utilizados en la clase de consultas, reduce nivel de complejidad ciclomatica de sonar|
|parametrosList(parametros)||
|validaTipos(parametro)|Se valida el tipo de parámetro que se esta enviando a DAOS. Se regresa un objecto de tipo sqlparameter con el tipo de dato requerido para que la ejecución contra la db sea correcta|
|validaEntrada(parametro)|Valida entrada|
|resultMaptoJson(resultMap)|Método para convertir objetos map string object en JsonObjects|
|resultMaptoJson2(resultMap)|Método para convertir objetos map string object en JsonObjects|
|validarEntry(entry)|Método para convertir objetos map string object en JsonObjects|
|creaArreglo(entry)|Método para convertir objetos map string object en JsonObjects|
|creaEntry2(entry)|Método para convertir objetos map string object en JsonObjects|
|parametrosBD(numero)|Método para convertir objetos map string object en JsonObjects|


### 5.2 Logs

La implementación de logs tiene la finalidad de monitorear la aplicación en tiempo real. Esto ayuda a detectar bugs de forma muy rápida. Es parte de las buenas prácticas implementar un log que nos mantenga informado sobre el status de la aplicación en cada fase de su ciclo de vida.

Dentro de este apartado tenemos la siguiente estructura:

- :open_file_folder: **log**
    - :coffee: Log.java

#### :coffee: **Log.java**

| Características | Descripción |
| ---             | ---         |
| Nombre de clase | Log |
|Descripción|Clase para el manejo de logs de las aplicaciones. Este logs es utilizado para guardar los archivos físicamente en los contenedores inside "application/logs" como ruta default|
|getFile()|Utilizado para recuperar el file de logs|
|setFile(fl)|Utilizado para setear el file de logs|
|getFileHandler()|Utilizado para recuperar el filehandler de logs|
|setFileHandler(fileHandler)|Utilizado para setear el filehandler de logs|
|depuraMensajes(cadena, bRespetaCase)|Método utilizado para depurar mensajes con palabras especiales para logs|
|escribeLog(guardaLog, lvl, aplicacion, servicio, peticion, respuesta, mensaje, e)|Método utilizado para escribir mensajes en log con fix de tiempo no superior a 1 segundo|
|escribeLogMS(guardaLog, lvl, aplicacion, servicio, peticion, respuesta, mensaje, e)|Método utilizado para escribir mensajes en log|
|escribeLogger(lvl, aplicacion, mensaje)|Utilizado para enviar a escribir en logger consola y archivo el mensaje enviado por app|


### 5.3 Seguridad

La implementación de la seguridad engloba diversos aspectos tales como: formatos definidos de fecha, mensajes de éxito y error, tratamiento de cadenas de texto, validaciones, definición de constantes, etc. No se limita sólo a implementar cifrados o contraseñas.

Dentro de este apartado tenemos la siguiente estructura:

- :open_file_folder: **security**
    - :coffee: APIGEE.java
    - :coffee: Cifrado.java
    - :coffee: CifradoDocumentos.java
    - :coffee: Cobranza.java

#### :coffee: Cifrado.java

| Características | Descripción |
| ---             | ---         |
| Nombre de clase | Cifrado |
|Descripción|Clase de seguridad para el manejo de los cifrados con las llaves de seguridad de cobranza|
|createKeyPrivate(encodePrivateKey)|Método utilizado para crear llaves privadas con RSA|
|createKeyPublic(encodePublicKey)|Método utilizado para crear llaves públicas con RSA|
|decryptMessage(encryptedText, privKy)|Método utilizado para decifrar mensajes con llave privada|
|encryptMessage(plainText, pubKy)|Método utilizado para cifrar mensajes con llave pública|

#### :coffee: CifradoDocumentos.java

| Características | Descripción |
| ---             | ---         |
| Nombre de clase | CifradoDocumentos |
|Descripción|Clase de seguridad para el manejo de los cifrados con las llaves de seguridad de cobranza|
|encryptDocument(valorCampo, aesKeyBase64, hmacKeyBase64)|Método utilizado para cifrar mensajes con llave pública|
|decryptDocument(valorCifrado, aesKeyBase64, hmacKeyBase64)|Método utilizado para descifrar mensajes con llave pública|

### 5.4 Cobranza

La implementación de esta clase gestiona el uso de las llaves de seguridad de la dirección de Investigación y Cobranza. Entre sus funciones está la generación de llaves de seguridad a tráves del consumo del api de llaves de seguridad, descifrar valores mediante un id de acceso y cifrar/descifrar cadenas de texto.

#### :coffee: Cobranza.java

| Características | Descripción |
| ---             | ---         |
| Nombre de clase | Cobranza |
|Descripción|Clase cobranza utilizda para el manejo de llas llaves de seguridad de la dirección de IyC|
|generaLlavesSeguridad()|Se utiliza para la generación de las llaves de seguridad a traves del consumo del api de llaves de seguridad de la dirección de IyC|
|getLlavesxID(idAcceso, jdbcTemplate)|Se utiliza para la recuperación de la contraparte de llaves de seguridad para poder descrifrar valores|
|cifrar(valor, origen, accion)|Se utiliza para cifrar/descrifrar cadenas de texto utilizando los diferentes origenes y llaves de seguridad de cobranza|
|cifrarDocumentos(valor, accion)|Se utiliza para cifrar/descrifrar documentos utilizando los diferentes origenes y llaves de seguridad de cobranza|

### 5.5 APIGEE

Es una clase que pertenece a la clasificación de seguridad cuya finalidad es el manejo de los consumos de access token hacia APIGEE.

#### :coffee: APIGEE.java

| Características | Descripción |
| ---             | ---         |
| Nombre de clase | APIGEE |
|Descripción|Clase de seguridad para el manejo de los consumos de access token hacia apigee|
|generaCredenciales()|Método utilizado para generar las credenciales de acceso para el consumo de apis cargadas a una cuenta app developer que fue utilizada con el constructor usr y pwd|

## 6. Jenkins File

```javascript
pipeline {
    environment {
        scannerHome=tool 'SonarQube'
    }
    agent any
    stages {
        stage('clean workspace'){
            steps {
                sh '''
                    ls 
                    echo $directorio
                    java -version  
                    rm -r -f /p/a/t/h Checkmarx
                    rm -r -f /p/a/t/h build
                '''
            }
        }
        stage('gradle tasks') {
            steps {
                script {
                if(offline == 'no') {
                    sh "${tool 'Gradle7.0'}/bin/gradle tasks"
                }
                else{
                    sh "${tool 'Gradle7.0'}/bin/gradle --offline tasks"
                }
              }
            }
        }
        stage('gradle build') {
            steps {
                 script {
                    if(offline == 'no') {
                        sh "${tool 'Gradle7.0'}/bin/gradle build -x test"
                    }
                    else{
                        sh "${tool 'Gradle7.0'}/bin/gradle --offline build -x test"
                    }
                }
            }
        }
        stage('gradle test') {
            steps {
                script {
                     if(offline == 'no') {
                        sh "${tool 'Gradle7.0'}/bin/gradle test"
                        sh '''
                            mkdir -p coverage
                            cp ./build/reports/jacoco/test/jacocoTestReport.xml ./coverage/jacoco.xml
                        '''
                    }
                    else{
                        sh "${tool 'Gradle7.0'}/bin/gradle --offline test"
                        sh '''
                            mkdir -p coverage
                            cp ./build/reports/jacoco/test/jacocoTestReport.xml ./coverage/jacoco.xml
                        '''
                    }
                }
            }
        }
        stage('SonarQube Analysis') {
            steps{
                withSonarQubeEnv('SonarQube') {
                    sh "${scannerHome}/bin/sonar-scanner -D'sonar.login=$sonarpoclogin'"
                }
            }
        }
        stage('checkmarx') {
            steps {
                script {
                    if(Checkmarx == 'si') {
                        step([$class: 'CxScanBuilder',
                        addGlobalCommenToBuildCommet: true,
                        comment: '',
                        configAsCode: true,
                        credentialsId: 'checkmarxcloud',
                        excludeFolders: '',
                        exclusionsSetting: 'global',
                        failBuildOnNewResults: false,
                        failBuildOnNewSeverity: 'HIGH',
                        filterPattern: '''!**/_cvs/**/*, !**/.svn/**/*, !**/.hg/**/*, !**/.git/**/*, !**/.bzr/**/*,
                            !**/.gitgnore/**/*, !**/.gradle/**/*, !**/.checkstyle/**/*, !**/.classpath/**/*, !**/bin/**/*,
                            !**/obj/**/*, !**/backup/**/*, !**/.idea/**/*, !**/*.DS_Store, !**/*.ipr, !**/*.iws,
                            !**/*.bak, !**/*.tmp, !**/*.aac, !**/*.aif, !**/*.iff, !**/*.m3u, !**/*.mid, !**/*.mp3,
                            !**/*.mpa, !**/*.ra, !**/*.wav, !**/*.wma, !**/*.3g2, !**/*.3gp, !**/*.asf, !**/*.asx,
                            !**/*.avi, !**/*.flv, !**/*.mov, !**/*.mp4, !**/*.mpg, !**/*.rm, !**/*.swf, !**/*.vob,
                            !**/*.wmv, !**/*.bmp, !**/*.gif, !**/*.jpg, !**/*.png, !**/*.psd, !**/*.tif, !**/*.swf,
                            !**/*.jar, !**/*.zip, !**/*.rar, !**/*.exe, !**/*.dll, !**/*.pdb, !**/*.7z, !**/*.gz,
                            !**/*.tar.gz, !**/*.tar, !**/*.gz, !**/*.ahtm, !**/*.ahtml, !**/*.fhtml, !**/*.hdm,
                            !**/*.hdml, !**/*.hsql, !**/*.ht, !**/*.hta, !**/*.htc, !**/*.htd, !**/*.war, !**/*.ear,
                            !**/*.htmls, !**/*.ihtml, !**/*.mht, !**/*.mhtm, !**/*.mhtml, !**/*.ssi, !**/*.stm,
                            !**/*.bin,!**/*.lock,!**/*.svg,!**/*.obj,
                            !**/*.stml, !**/*.ttml, !**/*.txn, !**/*.xhtm, !**/*.xhtml, !**/*.class, !**/*.iml, !Checkmarx/Reports/*.*,
                            !OSADependencies.json, !**/node_modules/**/*, !**/build/**, !**/gradle/**, !**/.gradle/**, !**/.idea/**''',
                        fullScanCycle: 10,
                        fullScansScheduled: true,
                        generatePdfReport: true,
                        groupId: '123',
                        incremental: true,
                        password: '{AQAAABAAAAAQmytvsivAP0KIfpxWFw7UNbjbR/QWPtDY0U+TdmDrurs=}',
                        preset: '36',
                        projectName: 'core',
                        sastEnabled: true,
                        serverUrl: 'https://10.64.248.39/',
                        sourceEncoding: '1',
                        useOwnServerCredentials: true,
                        username: '',
                        vulnerabilityThresholdEnabled: true,
                        vulnerabilityThresholdResult: 'UNSTABLE',
                        waitForResultsEnabled: true])
                    }
                    else{
                        echo 'El pipeline no solicitó revisión con checkmarx'
                        sh 'sleep 120'
                    }
                }
            }
        }
        /*stage("SonarQube QualityGate") {
            steps {
                timeout(time: 60, unit: 'SECONDS') {
                    waitForQualityGate (abortPipeline: true)
                }
            }
        }*/
        stage('gradle jar') {
                    steps {
                        sh "${tool 'Gradle7.0'}/bin/gradle jar"
                    }
                }
        stage('artifactory') {
            steps {
                script {
                    if(deploy == 'sí') {
                        sh "${tool 'Gradle7.0'}/bin/gradle publishAllPublicationsToDevopsRepository"
                    }
                }
            }
        }
        stage('archivos'){
            steps {
                step([$class: 'ArtifactArchiver', artifacts: '**/build/libs/*.jar', fingerprint: true])
            }
        }
    }
}
```






